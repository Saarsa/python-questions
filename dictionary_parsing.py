import json


def parse(string):
    return json.loads(string)


if __name__ == "__main__":
    assert parse('{"apple":4, "orange":5, "pear":6}') == {
        "apple": 4,
        "orange": 5,
        "pear": 6,
    }
