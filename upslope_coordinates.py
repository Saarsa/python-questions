def upslope(lis):
    output = []
    temp_index = 0
    delete_index = []
    for index, num in enumerate(lis):
        try:
            if num == lis[index + 1]:
                delete_index.append(index)
        except IndexError:
            pass
    for index, index_num in enumerate(delete_index):
        lis.pop(index_num - index)
    for index, num in enumerate(lis):
        if index == 0:
            continue
        if num == lis[index - 1]:
            temp_index += 1
        if num < lis[index - 1]:
            output.append(lis[temp_index:index])
            temp_index = index
    else:
        output.append(lis[temp_index : index + 1])
    return output


if __name__ == "__main__":
    assert upslope([1, 3, 3, 4, 6, 2, 2, 3, 5, 3, 3, 8, 9, 9, 1, 1]) == [
        [1, 3, 4, 6],
        [2, 3, 5],
        [3, 8, 9],
        [1],
    ]
