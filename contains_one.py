def contains1(num):
    num_list = []
    while num > 0:
        num, remd = divmod(num, 10)
        num_list.append(remd)
    return 1 in num_list


if __name__ == "__main__":
    assert contains1(1245)
    assert not contains1(245)
