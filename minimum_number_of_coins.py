def min_coins(coins, vaule):
    answer = {}
    coins.sort()
    coins.reverse()
    for coin in coins:
        if int(vaule / coin):
            answer[coin] = int(vaule / coin)
            vaule -= int(vaule / coin) * coin
    if vaule != 0:
        return {}
    return answer


if __name__ == "__main__":
    assert min_coins([1, 2, 5], 101) == {5: 20, 1: 1}
    assert min_coins([2, 3], 20) == {3: 6, 2: 1}
    assert min_coins([2, 4, 6], 5) == {}
