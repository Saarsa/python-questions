### 5) Upslope Coordinates
`lis = [1, 3, 4, 6, 2, 3, 5, 3, 3, 8, 9]`

You are given a list of numbers representing land height. Write a function

`upslope(lis)` that takes in this list of numbers, and returns a 2D list

Containing sections that are upslope (increasing).
```
x = upslope(lis)

# [[1, 3, 4, 6], [2, 3, 5], [3, 8, 9]]
```
How to use the function
```
from upslope_coordinates import upslope
lis = [1, 3, 3, 4, 6, 2, 2, 3, 5, 3, 3, 8, 9, 9, 1,1]
answer = upslope(lis)
print(answer)
# [[1, 3, 4, 6], [2, 3, 5], [3, 8, 9], [1]]
```


### 6) Contains 1 (No Strings Allowed)
Write a function `contains1(n)` that takes in an integer `n`, and returns True if

n contains the digit 1, and False otherwise. You cannot use any strings or 

String methods. (if you could, it’s way too easy)

```
contains1(21)    # True
contains1(201)   # True
contains1(617)   # True

contains1(22)    # False
contains1(202)   # False
contains1(627)   # False
```
How to use the function
```
from contains_one import contains1
num1 = 1245
num2 = 245
answer1 = contains1(num1)
answer2 = contains1(num2)
print(f"{answer1=}", f"{answer2=}")
# answer1=True answer2=False
```

### 7) Minimum number of coins
Write a function `min_coins(coins, value)` that takes in a list `coins` and an

integer value, and returns a dictionary representing the minimum total

number of coins we need to make up `value`. For instance:
```
min_coins([1, 2, 5], 101)
```

* We have an infinite number of $1 coins, $2 coins and $5 coins
* We need to find the minimum number of coins to make $101
* In this case, the best possible answer is 20 $5 coins and 1 $1 coin
* The function hence returns `{1:1, 5:20}`

```
min_coins([2, 3], 20)
```

* We have an infinite number of $2 and $3 coins
* We need the minimum number of coins to make up $20
* The answer is 6 $3 coins and 1 $2 coin.
* The function hence returns `{2:1, 3:6}`

```
min_coins([2, 4, 6], 5)
```
* We have an infinite number of $2, $4 and $6 coins to make $5
* This is not possible, so we simply return `{}`

```
from minimum_number_of_coins import min_coins
assert min_coins([1, 2, 5], 101) == {5: 20, 1: 1}
assert min_coins([2, 3], 20) == {3:6, 2:1}
assert min_coins([2, 4, 6], 5) == {}

```
